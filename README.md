Ansible Role: Pgpool
===================
Install and configure Redis on Debian servers.

----------

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

    redis_bind: "127.0.0.1"
    redis_port: "6379"

    redis_tcp_backlog: "511"
    redis_connection_timeout: "0"
    redis_tcp_keepalive: "300"

    redis_demonize: "yes"
    redis_loglevel: notice

    redis_logfile: /var/log/redis/redis-server.log

    redis_databases: 16

    redis_saves:
        - seconds: 900
          changes: 1
        - seconds: 300
          changes: 10
        - seconds: 60
          changes: 10000
          
    redis_stop_writes_on_bgsave_error: "yes"
    redis_rdbcompression: "yes"
    redis_rdbchecksum: "yes"
    redis_dbfilename: dump.rdb

    redis_dir: "/var/lib/redis/"

These vars are all related to the Redis config file check [here](https://redis.io/topics/config) for more information.

Particular attention to `redis_save` var:
It save the DB on disk:  save `<seconds>` `<changes>` will save the DB if both the given number of seconds and the given number of write operations against the DB occurred.

In the defaults above the behavior will be to save:

- after 900 sec (15 min) if at least 1 key changed
- after 300 sec (5 min) if at least 10 keys changed
- after 60 sec if at least 10000 keys changed

Note: you can disable saving not declaring any element.

Additional optional parameter is 

	redis_password:
If set this enable password on Redis, so you can bind to different ip than `localhost`.

**Warning**: since Redis is pretty fast an outside user can try up to 150k passwords per second against a good box. This means that you should use a very strong password (at least 64 character) otherwise it will be very easy to break. 
   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: install redis
      hosts: all
      vars:
          redis_password: '{{ vault_redis_password }}'
          redis_bind: '*'
	      redis_saves:
	          - seconds: 900
                changes: 1
              - seconds: 300
                changes: 10
              - seconds: 60
                changes: 10000
      roles:
        - redis

